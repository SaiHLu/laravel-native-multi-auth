<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest:admin')->except(['logout']);
    }

    public function showLoginForm()
    {
        return view('auth.admin-login');
    }

    public function login(Request $request)
    {
        // dd($request);
        // Validate the form
        $rules = [
            'email' => 'required|email|max:255',
            'password' => 'required|string|max:255',
        ];

        $messages = [
            'required' => 'These credentials do not match our records.'
        ];

        $request->validate($rules, $messages);

        /**
         * Attempt Admin Login
         * */

        if (Auth::guard('admin')->attempt($request->only(['email', 'password']), $request->remember)) {
            // If success then redirect user to intended location
            return redirect()->intended(route('admin.dashboard'));
        }

        // If Fail then redirect back to login
        return redirect()->back()->withInput($request->only(['email', 'remember']));
    }

    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();


        return  redirect('/');
    }
}
